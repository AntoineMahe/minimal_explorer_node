#!/usr/bin/env python
import math
import numpy as np
from scipy import optimize
import rospy
from sensor_msgs.msg import MagneticField

class CompassCalibration():
    def __init__(self):
        self.mag_sub = rospy.Subscriber('mag', MagneticField, self.on_mag, queue_size=1)
        #array for mag data
        self.magx = np.array([])
        self.magy = np.array([])
        self.recording = False


    def on_mag(self,msg):
        x = msg.magnetic_field.x
        y = msg.magnetic_field.y
        if self.recording:
            self.magx = np.append(self.magx,x)
            self.magy = np.append(self.magy,y)

    def calc_R(self,xc, yc):
        """ calculate the distance of each 2D points from the center (xc, yc) """
        return np.sqrt((self.magx-xc)**2 + (self.magy-yc)**2)

    def f_2(self,c):
        """ calculate the algebraic distance between the data points and the mean circle centered at c=(xc, yc) """
        Ri = self.calc_R(*c)
        return Ri - Ri.mean()


    def collect_mag_data(self):
        #wait for operator to start making a circle
        raw_input("Press Enter as you start making a circle")
        self.recording = True

        #wait for operator confirmation
        raw_input("Press Enter when the circle is done")

        self.recording = False



    def compute_mag_offset(self):
        center_estimate = np.mean(self.magx), np.mean(self.magy)

        center_2, ier = optimize.leastsq(self.f_2, center_estimate)

        xc_2, yc_2 = center_2

        rospy.loginfo("mag offset x : " + str(xc_2))
        rospy.loginfo("mag offset y : " + str(yc_2))
        rospy.set_param('/mag_offset_x',float(xc_2))
        rospy.set_param('/mag_offset_y',float(yc_2))
        #set param offset

if __name__ == '__main__':
    rospy.init_node('compasscalibration', anonymous=True)
    node = CompassCalibration()

    node.collect_mag_data()
    node.compute_mag_offset()
