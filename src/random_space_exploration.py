#!/usr/bin/env python
import math
import numpy as np
import roslib; roslib.load_manifest('minimal_explorer_node')
import rospy
from nav_msgs.msg import Odometry
from geometry_msgs.msg import Pose
from kingfisher_msgs.msg import Drive
from geometry_msgs.msg import Vector3Stamped
from geometry_msgs.msg import Twist
from visualization_msgs.msg import Marker


class RandomWalk():
    def __init__(self):
        self.pub_drive = rospy.Publisher("cmd_drive", Drive)
        self.pub_vel = rospy.Publisher("cmd_vel_test", Twist)
        self.odom_sub = rospy.Subscriber('odom', Odometry, self.on_odom, queue_size=1)
        self.rpy_sub = rospy.Subscriber('imu_rpy', Vector3Stamped, self.on_rpy, queue_size=1)

        self.aggres = 0.5
        self.rate = 20
        self.origin = Pose()#starting coordinate
        self.odom = Odometry()
        self.rpy = Vector3Stamped()
        self.Kp_x = 0.3
        self.Kp_rz = 20
        self.Kd_rz = 0.2

    def on_odom(self, msg):
        self.odom = msg

    def on_rpy(self, msg):
        self.rpy = msg

    def publish_once_in_cmd_drive(self, cmd):
        """
        This is because publishing in topics sometimes fails the first time you publish.
        In continuos publishing systems there is no big deal but in systems that publish only
        once it IS very important.
        """
        rate_pub = rospy.Rate(self.rate)
        while True:
            connections = self.pub_drive.get_num_connections()
            if connections > 0:
                self.pub_drive.publish(cmd)
                rospy.logdebug("Cmd Published")
                break
            else:
                rate_pub.sleep()

    def publish_once_in_cmd_vel(self, cmd):
        """
        This is because publishing in topics sometimes fails the first time you publish.
        In continuos publishing systems there is no big deal but in systems that publish only
        once it IS very important.
        """
        rate_pub = rospy.Rate(self.rate)
        while True:
            connections = self.pub_vel.get_num_connections()
            if connections > 0:
                self.pub_vel.publish(cmd)
                rospy.logdebug("Cmd Published")
                break
            else:
                rate_pub.sleep()

    def uniqangle(self,angle):
        "project angle in -pi pi"

        tmp_angle = math.fmod(angle, 2 * math.pi)#angle between 0 and 2pi

        if tmp_angle <= math.pi:
            return tmp_angle
        else:
            return tmp_angle - 2 * math.pi

    def draw_point(self, pose, color):

        target = Marker()
        target.header.frame_id = "world"
        target.type = target.SPHERE
        target.action = target.ADD
        target.lifetime = rospy.Duration(0)
        target.scale.x = 1
        target.scale.y = 1
        target.scale.z = 1
        target.color.a = 1.0

        target.color.r = 0.0
        target.color.g = 0.0
        target.color.b = 0.0

        if color == "red":
            target.color.r = 1
            target.id = 1
        if color == "blue":
            target.color.b = 1
            target.id = 2
        if color == "green":
            target.color.g = 1
            target.id = 3
        target.pose = pose

        self._target.publish(target)

    def goto(self, coord):
        rospy.loginfo("debug : goto : start")
        rospy.loginfo("debug : goto : position :")
        rospy.loginfo(self.odom)
        err_d = 10
        rate_pub = rospy.Rate(self.rate)
        err_angle = 0

        while err_d > 4.:#tant que l'on est a plus de 2 m de l'objectif
            #pid to go to coord

            x = coord.position.x - self.odom.pose.pose.position.x
            y = coord.position.y - self.odom.pose.pose.position.y
            d = np.linalg.norm([x,y])
            err_d = d
            rospy.loginfo("debug goto : d : " + str(d) )
            y_n = y/d
            x_n = x/d
            # - calculate theta angle satisfaying previous condition
            # use atan2 which do exactly what we want
            theta_target = math.atan2(y, x) #this is the angle from target to current position


            #theta_target = theta_target#  + math.pi
            #TODO around pi the target go from pi to -pi, need to stabilized that

            #theta_target = self.uniqangle(theta_target)
            #theta_mes = self.rpy.vector.z
            qz = self.odom.pose.pose.orientation.z
            qw = self.odom.pose.pose.orientation.w
            theta_mes = 2.* math.atan2(qz,qw)
            rospy.logdebug("theta_target : " + str(theta_target))
            rospy.logdebug("theta_current : " + str(theta_mes))
            p_err_angle = err_angle

            err_angle = theta_target - theta_mes

            if abs(err_angle)>math.pi:
                err_angle = 2 * math.pi - err_angle


            cmd = Twist()

            if abs(err_angle)<math.pi/6.0:
                cmd.linear.x =  err_d * self.Kp_x
            else:
                cmd.linear.x = 0
            cmd.angular.z = err_angle * self.Kp_rz# + d_err_angle * self.Kd_rz

            
            # scaling factors
            linfac = 0.2
            angfac = 0.05

            cmd_drive = Drive()

            cmd_drive.left = linfac * cmd.linear.x - angfac * cmd.angular.z
            cmd_drive.right = linfac * cmd.linear.x + angfac * cmd.angular.z
            
            #cmd clipping

            if abs(cmd_drive.left) > 0.8:
                cmd_drive.left = 0.8 * cmd_drive.left/abs(cmd_drive.left)
            if abs(cmd_drive.left) < 0.2:
                cmd_drive.left = 0.2 * cmd_drive.left/abs(cmd_drive.left)

            if abs(cmd_drive.right) > 0.8:
                cmd_drive.right = 0.8 * cmd_drive.right/abs(cmd_drive.right)
            if abs(cmd_drive.right) < 0.2:
                cmd_drive.right = 0.2 * cmd_drive.right/abs(cmd_drive.right)
            
            self.publish_once_in_cmd_drive(cmd_drive)
            rate_pub.sleep()
            #turn such that current pose give the head toward goal
            # - asserving theta angle to the calculated one

            #calculate a twist then send it to twist2drive node.

        rospy.loginfo("debug : goto : end")
        return


    def distance(self):
        p = np.array([self.odom.pose.pose.position.x - self.origin.position.x, self.odom.pose.pose.position.y - self.origin.position.y])
        return np.linalg.norm(p)

    def go_home(self):
        self.goto(self.origin)

    def do_rectangle(self):
        coord = Pose()
        coord.position.x = 10
        coord.position.y = 5
        self.goto(coord)
        coord.position.x = 10
        coord.position.y = -5
        self.goto(coord)
        coord.position.x = -10
        coord.position.y = -5
        self.goto(coord)
        coord.position.x = -10
        coord.position.y = 5
        self.goto(coord)
        coord.position.x = 10
        coord.position.y = 5
        self.goto(coord)


    def walk(self):
        rate_pub = rospy.Rate(self.rate)
        rospy.loginfo("walk beginning in 5s")
        rospy.sleep(5)

        cmd = Drive()
        n = 5#second during which we apply the cmd
        time_new_cmd = n * self.rate #next cmd every n second
        time_counter = 0
        self.origin = self.odom.pose.pose
        rospy.login("origin : " + str(self.origin))
        while True:
            d = self.distance()
            if d>20.:
                self.go_home()
            #draw a thruster cmd @todo : generating sequence of command so they are more "natural" (closer to what we may observed on the system)
            old_min, old_max = 0., 1.
            new_min, new_max = 0.2, 0.8
            if time_counter == time_new_cmd:
                l = np.random.random()
                r = np.random.random()
                signl = np.random.randint(2)
                signr = np.random.randint(2)
                
                cmd.left =  new_min + (l -
                        old_min)*(new_max-new_min)/(old_max-old_min)
                cmd.right = new_min + (r -
                        old_min)*(new_max-new_min)/(old_max-old_min)
                
                cmd.left = (-1)**signl * cmd.left
                cmd.right = (-1)**signr * cmd.right

                time_counter = 0
            
            #apply the cmd for a significant time
            self.publish_once_in_cmd_drive(cmd)
            rate_pub.sleep()
            time_counter += 1

    def walk_smooth(self):
        rate_pub = rospy.Rate(self.rate)
        rospy.loginfo("walk beginning in 5s")
        rospy.sleep(5)
        cmd = Drive()
        old_cmd = Drive()
        new_cmd = Drive()
        n = 5#second during which we apply the cmd todo : randomized
        time_new_cmd_l = n * self.rate #next cmd every n second
        time_new_cmd_r = n * self.rate #next cmd every n second
        time_counter_l = 0
        time_counter_r = 0
        self.origin = self.odom.pose.pose
        rospy.loginfo("origin : " + str(self.origin))
        #todo separating time for left and rigth
        while True:
            d = self.distance()
            if d>20.:
                self.go_home()
            #draw a thruster cmd @todo : generating sequence of command so they are more "natural" (closer to what we may observed on the system)
            old_min, old_max = 0., 1.
            new_min, new_max = 0., 0.8
            if time_counter_l >= time_new_cmd_l:
                l = np.random.random()
                signl = np.random.randint(2)
                
                old_cmd = cmd #for smooth transition between old and new cmd

                #todo smoothing more subtily
                new_cmd.left =  new_min + (l -
                        old_min)*(new_max-new_min)/(old_max-old_min)
                
                new_cmd.left = (-1)**signl * new_cmd.left

                time_counter_l = 0
                time_new_cmd_l = 5. + 2 * np.random.random()
                time_new_cmd_l = time_new_cmd_l * self.rate

                
            if time_counter_r >= time_new_cmd_r:
                r = np.random.random()
                signr = np.random.randint(2)
                
                old_cmd = cmd #for smooth transition between old and new cmd

                #todo smoothing more subtily
                new_cmd.right = new_min + (r -
                        old_min)*(new_max-new_min)/(old_max-old_min)
                
                new_cmd.right = (-1)**signr * new_cmd.right

                time_counter_r = 0
                time_new_cmd_r = 5. + 2 * np.random.random()
                time_new_cmd_r = time_new_cmd_r * self.rate
            
            #one second transition
            cmd.left = old_cmd.left + (new_cmd.left-old_cmd.left) * min(float(time_counter_l)/self.rate,1.)
            cmd.right = old_cmd.right + (new_cmd.right-old_cmd.right) * min(float(time_counter_r)/self.rate,1.)
                
            #apply the cmd for a significant time
            self.publish_once_in_cmd_drive(cmd)
            rate_pub.sleep()
            time_counter_l += 1
            time_counter_r += 1





if __name__ == '__main__':
    rospy.init_node('randomwalk', anonymous=True)
    node = RandomWalk()
    while not rospy.is_shutdown():
        node.walk_smooth()
