#!/usr/bin/python


import rospy
from math import sin,cos,pi,atan2
from nav_msgs.msg import Odometry
from sensor_msgs.msg import Imu
from gps_common.msg import GPSFix
from geometry_msgs.msg import Point
from geometry_msgs.msg import Vector3Stamped
from sensor_msgs.msg import MagneticField
import tf


mag_offset_x=None
mag_offset_y=None

utmInit=None
odom=Odometry()
odomPub=None
rpy_pub=None
broadcaster = tf.TransformBroadcaster()
useCompassHeading = True

def imuCallback(msg):
    global odom, odomPub, rpy_pub, broadcaster, utmInit
    theta = 0.0
    m = msg.orientation
    odom.pose.pose.orientation = m

    v = tf.transformations.euler_from_quaternion([m.x,m.y,m.z,m.w])
    theta = v[2]

    rpy = Vector3Stamped()
    rpy.header = msg.header
    rpy.vector.x = v[0]
    rpy.vector.y = v[1]
    rpy.vector.z = v[2]

    rpy_pub.publish(rpy)

    odomPub.publish(odom)

    p = (-utmInit.x, -utmInit.y, -utmInit.z)
    q = tf.transformations.quaternion_from_euler(0, 0, 0)

    broadcaster.sendTransform(p, q, msg.header.stamp, "/somewhere_in_bern", "/kingfisher/gps_odom")

    p = (-cos(-theta)*odom.pose.pose.position.x + sin(-theta)*odom.pose.pose.position.y,
            -sin(-theta)*odom.pose.pose.position.x - cos(-theta)*odom.pose.pose.position.y,
            -odom.pose.pose.position.z)
    q = tf.transformations.quaternion_from_euler(0, 0, -theta)

    broadcaster.sendTransform(p, q, msg.header.stamp, "/kingfisher/gps_odom", "/kingfisher/base")


def compassCallback(msg):
    global odom,odomPub,broadcaster, utmInit, useCompassHeading, mag_offset_x, mag_offset_y
    if utmInit is None:
        return
    odom.header.stamp = msg.header.stamp
    theta = 0.0
    if useCompassHeading:
        x = msg.magnetic_field.x
        y = msg.magnetic_field.y
        z = msg.magnetic_field.z

        #theta from magnetic fields + correction for bias in mag mesurment

        #@TODO finer 3D estimation of mag field bias (first the estimation done by considering the mag field is in the xy plan and making a approximation of the mag field origin by visual estimation.

        xn = x - mag_offset_x
        yn = y - mag_offset_y
        #we don't really care about z because only yaw is of interest for us
        zn = z

        #theta = atan2(yn,xn)
        #90 rotation for aligning gps_mag heading and boat heading
        theta = atan2(xn,-yn)

        odom.pose.pose.orientation.x = 0
        odom.pose.pose.orientation.y = 0
        odom.pose.pose.orientation.z = sin(theta/2)
        odom.pose.pose.orientation.w = cos(theta/2)
    else:
        theta = atan2(odom.pose.pose.orientation.z,odom.pose.pose.orientation.w)*2;

    odomPub.publish(odom)

    p = (-utmInit.x, -utmInit.y, -utmInit.z)
    q = tf.transformations.quaternion_from_euler(0, 0, 0)
    broadcaster.sendTransform(p, q, msg.header.stamp, "/somewhere_in_bern", "/kingfisher/gps_odom")
    p = (-cos(-theta)*odom.pose.pose.position.x + sin(-theta)*odom.pose.pose.position.y,
            -sin(-theta)*odom.pose.pose.position.x - cos(-theta)*odom.pose.pose.position.y,
            -odom.pose.pose.position.z)
    q = tf.transformations.quaternion_from_euler(0, 0, -theta)
    broadcaster.sendTransform(p, q, msg.header.stamp, "/kingfisher/gps_odom", "/kingfisher/base")

def gpsFixCallback(msg):
    global odom,odomPub,broadcaster, utmInit, useCompassHeading
    if utmInit is None:
        return
    # odom.header.stamp = msg.header.stamp
    theta = 0.0
    if not useCompassHeading:
        theta = pi/2 - msg.track
        odom.pose.pose.orientation.x = 0
        odom.pose.pose.orientation.y = 0
        odom.pose.pose.orientation.z = sin(theta/2)
        odom.pose.pose.orientation.w = cos(theta/2)
    else:
        theta = atan2(odom.pose.pose.orientation.z,odom.pose.pose.orientation.w)*2;
    odom.twist.twist.linear.x = msg.speed
    odom.twist.twist.linear.y = 0
    odom.twist.twist.linear.z = 0

    odomPub.publish(odom)

    p = (-utmInit.x, -utmInit.y, -utmInit.z)
    q = tf.transformations.quaternion_from_euler(0, 0, 0)
    broadcaster.sendTransform(p, q, odom.header.stamp, "/somewhere_in_bern", "/kingfisher/gps_odom")
    p = (-cos(-theta)*odom.pose.pose.position.x + sin(-theta)*odom.pose.pose.position.y,
            -sin(-theta)*odom.pose.pose.position.x - cos(-theta)*odom.pose.pose.position.y,
            -odom.pose.pose.position.z)
    q = tf.transformations.quaternion_from_euler(0, 0, -theta)
    broadcaster.sendTransform(p, q, odom.header.stamp, "/kingfisher/gps_odom", "/kingfisher/base")

def utmCallback(msg):
    global odom, odomPub, utmInit
    odom.header.stamp = msg.header.stamp
    if utmInit is None:
        utmInit = msg.pose.pose.position
        rospy.loginfo("GPS2ODOM: GPS origin set from sensor to\n" + str(utmInit))
        open("gps_origin.csv","w").write("%f,%f,%f\n"%(utmInit.x,utmInit.y,utmInit.z))
        #rosparam
        rospy.set_param('/utmInit_x',utmInit.x)
        rospy.set_param('/utmInit_y',utmInit.y)
        rospy.set_param('/utmInit_z',utmInit.z)
    odom.pose.pose.position.x = msg.pose.pose.position.x - utmInit.x
    odom.pose.pose.position.y = msg.pose.pose.position.y - utmInit.y
    odom.pose.pose.position.z = msg.pose.pose.position.z - utmInit.z

    odomPub.publish(odom)

    theta = atan2(odom.pose.pose.orientation.z,odom.pose.pose.orientation.w)*2;
    p = (-utmInit.x, -utmInit.y, -utmInit.z)
    q = tf.transformations.quaternion_from_euler(0, 0, 0)
    broadcaster.sendTransform(p, q, msg.header.stamp, "/somewhere_in_bern", "/kingfisher/gps_odom")
    p = (-cos(-theta)*odom.pose.pose.position.x + sin(-theta)*odom.pose.pose.position.y,
            -sin(-theta)*odom.pose.pose.position.x - cos(-theta)*odom.pose.pose.position.y,
            -odom.pose.pose.position.z)
    q = tf.transformations.quaternion_from_euler(0, 0, -theta)
    broadcaster.sendTransform(p, q, msg.header.stamp, "/kingfisher/gps_odom", "/kingfisher/base")




if __name__ == '__main__':
    global odom, odomPub, rpy_pub, broadcaster, utmInit, mag_offset_x, mag_offset_y
    rospy.init_node('gps2odom')
    odom.header.frame_id = rospy.get_param('~frame_id','kingfisher/gps_odom')
    useCompassHeading = rospy.get_param('~use_compass_heading',useCompassHeading)
    gps_origin_file = rospy.get_param('~gps_origin','')

    if not rospy.has_param('/mag_offset_x') or not rospy.has_param('/mag_offset_y'):
        rospy.logwarn("mag offset not set, using 22., 4.")
        # usually x offset is between 20 and 25 and y offset is between 0 and 10
        mag_offset_x = 22.
        mag_offset_y = 4.
    else:
        mag_offset_x = rospy.get_param('/mag_offset_x',mag_offset_x)
        mag_offset_y = rospy.get_param('/mag_offset_y',mag_offset_y)
        rospy.loginfo("mag offset set, using : " + str(mag_offset_x) + " , " + str(mag_offset_y) )


    if len(gps_origin_file) > 0:
        gps_origin = None
        try:
            gps_origin = open(gps_origin_file,"r").readlines()[0].strip().split(",")
        except:
            pass
        gps_origin = [float(s) for s in gps_origin]
        utmInit = Point(gps_origin[0],gps_origin[1],gps_origin[2])
        rospy.loginfo("GPS2ODOM: GPS origin set from file to\n" + str(utmInit))

    odomPub = rospy.Publisher('rtk/odom',Odometry,queue_size=1)
    rpy_pub = rospy.Publisher("rtk/imu/rpy", Vector3Stamped, queue_size=1)
    compassSub = rospy.Subscriber('~compass', MagneticField, compassCallback, queue_size=1)
    #gpsSub = rospy.Subscriber('~gps', GPSFix, gpsFixCallback, queue_size=1) no need using utm anyway (?)
    utmSub = rospy.Subscriber('~utm', Odometry, utmCallback, queue_size=1)
    imuSub = rospy.Subscriber('~imu', Imu, imuCallback, queue_size=1) #

    rospy.spin()






